//
//  CheckoutPaymentViewController.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

private let kLeftBarButtonTitleKey = "CheckoutPaymentViewController.LeftBarButton.title".localize()
private let kRightBarButtonTitleKey = "CheckoutPaymentViewController.RightBarButton.title".localize()

private let kAmountSectionHeaderTitleKey = "CheckoutPaymentViewController.AmountSection.HeaderCell.title".localize()
private let kAmountSectionChangeTitleKey = "CheckoutPaymentViewController.AmountSection.AmountCell.change".localize()

private let kPaymentSectionHeaderTitleKey = "CheckoutPaymentViewController.PaymentSection.HeaderCell.title".localize()
private let kPaymentSectionBankTitleKey = "CheckoutPaymentViewController.PaymentSection.BankCell.title".localize()
private let kPaymentSectionCreditCardTitleKey = "CheckoutPaymentViewController.PaymentSection.CreditCardCell.title".localize()
private let kPaymentSectionInstallmentsTitleKey = "CheckoutPaymentViewController.PaymentSection.InstallmentsCell.title".localize()
private let kPaymentSectionSelectKey = "CheckoutPaymentViewController.PaymentSection.Select".localize()

private let kPaymentSectionCardNotSelectedAlertTitle = "CheckoutPaymentViewController.PaymentSection.CreditCardCell.Alert.title".localize()
private let kPaymentSectionCardNotSelectedAlertMessage = "CheckoutPaymentViewController.PaymentSection.CreditCardCell.Alert.message".localize()
private let kPaymentSectionBankNotSelectedAlertTitle = "CheckoutPaymentViewController.PaymentSection.BankCell.Alert.title".localize()
private let kPaymentSectionBankNotSelectedAlertMessage = "CheckoutPaymentViewController.PaymentSection.BankCell.Alert.message".localize()
private let kPaymentSectionInstallmentNotSelectedAlertTitle = "CheckoutPaymentViewController.PaymentSection.InstallmentCell.Alert.title".localize()
private let kPaymentSectionInstallmentNotSelectedAlertMessage = "CheckoutPaymentViewController.PaymentSection.InstallmentCell.Alert.message".localize()

class CheckoutPaymentViewController: CheckoutBaseViewController {
    // MARK: - Datasource vars.
    private var paymentSections = Array<Array<PaymentCellModel>>()
    
    // MARK: - View life cycle functions.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationButtons()
        self.setTableFooterView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.paymentSections.removeAll()
        self.createSelectedAmountSection()
        self.createPaymentSection()
        self.baseTableView!.reloadData()
    }
    
    // MARK: - Styling functions.
    private func setNavigationButtons() {
        let leftButtonTitle = kLeftBarButtonTitleKey
        let rightButtonTitle = kRightBarButtonTitleKey
        
        let leftButton = UIBarButtonItem(title: leftButtonTitle, style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelAction))
        let rightButton = UIBarButtonItem(title: rightButtonTitle, style: UIBarButtonItem.Style.plain, target: self, action:#selector(confirmAction))
        
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    private func setTableFooterView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.baseTableView!.frame.size.width, height: kTableViewFooterHeight))
        
        let footerButton = UIButton(type: UIButton.ButtonType.system)
        footerButton.setTitle(kRightBarButtonTitleKey, for: UIControl.State.normal)
        footerButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        footerButton.addTarget(self, action: #selector(confirmAction), for: UIControl.Event.touchUpInside)
        footerButton.backgroundColor = UIColor.mlBlueColor()
        footerButton.roundedCorner()
        
        footerView.addSubview(footerButton)
        footerButton.constraintsMarginToSuperView(topMargin: Float(kTableViewFooterHeight - kButtonHeight),
                                                  leftMargin: Float(kMargin),
                                                  bottomMargin: 0,
                                                  rightMargin: Float(kMargin))
        
        self.baseTableView!.tableFooterView = footerView
    }
    
    // MARK: - Actions functions.
    @IBAction func cancelAction(sender: UIBarButtonItem) {
        ContextManager.sharedInstance.contextManagerReset()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func checkContext() -> (Bool, String?, String?) {
        let paymentMethod = ContextManager.sharedInstance.contextCreditCard
        let paymentBank = ContextManager.sharedInstance.contextBank
        let paymentInstallment = ContextManager.sharedInstance.contextInstallment
        
        var paymentAlertTitle = ""
        var paymentAlertMessage = ""
        
        if paymentMethod == nil {
            paymentAlertTitle = kPaymentSectionCardNotSelectedAlertTitle
            paymentAlertMessage = kPaymentSectionCardNotSelectedAlertMessage
            return (false, paymentAlertTitle, paymentAlertMessage)
        } else if paymentBank == nil {
            paymentAlertTitle = kPaymentSectionBankNotSelectedAlertTitle
            paymentAlertMessage = kPaymentSectionBankNotSelectedAlertMessage
            return (false, paymentAlertTitle, paymentAlertMessage)
        } else if paymentInstallment == nil {
            paymentAlertTitle = kPaymentSectionInstallmentNotSelectedAlertTitle
            paymentAlertMessage = kPaymentSectionInstallmentNotSelectedAlertMessage
            return (false, paymentAlertTitle, paymentAlertMessage)
        }
        
        return (true, nil, nil)
    }
    @IBAction func confirmAction(sender: UIBarButtonItem) {
        let (validContext, paymentAlertTitle, paymentAlertMessage) = self.checkContext()
        
        if validContext == true {
            self.navigationController?.popViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationPaymentSelected), object: nil)
        } else {
            let alert = UIAlertController(title: paymentAlertTitle, message: paymentAlertMessage, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: kAlertButtonOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view reimplemented functions.
    override func cellModel(indexPath: NSIndexPath) -> PaymentCellModel {
        let sectionList = self.paymentSections[indexPath.section]
        return sectionList[indexPath.row]
    }
    
    override func tableViewRowsNumber(section: Int) -> Int {
        guard section < self.paymentSections.count else {
            return 0
        }
        
        let sectionList = self.paymentSections[section]
        return sectionList.count
    }
    
    // MARK: - UITableViewDataSource implementation.
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.paymentSections.count
    }
    
    // MARK: - UITableViewDelegate implementation.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionList = self.paymentSections[indexPath.section]
        let cellModel = sectionList[indexPath.row]
        
        cellModel.paymentCellClousureAction!()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionList = self.paymentSections[indexPath.section]
        let cellModel = sectionList[indexPath.row]
        
        return InformationCell.cellHeight(cellModel: cellModel)
    }
    
    // MARK: - Datasource functions.
    private func createSelectedAmountSection() {
        let headerCellModel = PaymentCellModel(descriptionText: kAmountSectionHeaderTitleKey)
        let amountCellModel = PaymentCellModel(descriptionText: "$" + String(Int(ContextManager.sharedInstance.contextAmount!)),
                                               disclosureDescription: kAmountSectionChangeTitleKey,
                                               creditCardImage: nil,
                                               showDisclosure: true,
                                               action: {[unowned self] in
                                                let selectAmountViewController = CheckoutSelectAmountViewController(backAction: { [unowned self] in
                                                    self.navigationController?.popViewController(animated: true)
                                                    })
                                                
                                                self.navigationController?.pushViewController(selectAmountViewController, animated: true)
            })
        
        let amountSectionList = [headerCellModel, amountCellModel]
        self.paymentSections.append(amountSectionList)
    }
    
    private func createPaymentSection() {
        // Header cell.
        let headerCellModel = PaymentCellModel(descriptionText: kPaymentSectionHeaderTitleKey)
        
        // Credit card cell.
        var creditCardDescriptionText = kPaymentSectionCreditCardTitleKey
        var creditCardDisclosureText = kPaymentSectionSelectKey
        var creditCardImageName = ""
        
        if let creditCard = ContextManager.sharedInstance.contextCreditCard {
            creditCardDescriptionText = creditCard.paymentMethodName
            creditCardDisclosureText = ""
            creditCardImageName = creditCard.paymentMethodImageURL
        }
        
        let creditCardCellModel = PaymentCellModel(descriptionText: creditCardDescriptionText,
                                                   disclosureDescription: creditCardDisclosureText,
                                                   creditCardImage: creditCardImageName,
                                                   showDisclosure: true,
                                                   action: {[unowned self] in
                                                    self.navigationController?.pushViewController(CheckoutSelectCreditCardViewController(), animated: true)
            })
        
        // Bank cell.
        var bankDescriptionText = kPaymentSectionBankTitleKey
        var bankDisclousureText = kPaymentSectionSelectKey
        var bankImageName = ""
        
        if let bank = ContextManager.sharedInstance.contextBank {
            bankDescriptionText = bank.paymentBankName
            bankDisclousureText = ""
            bankImageName = bank.paymentBankImage
        }
        
        let bankCellModel = PaymentCellModel(descriptionText: bankDescriptionText, disclosureDescription: bankDisclousureText, creditCardImage: bankImageName, showDisclosure: true, action: { [unowned self] in
            if let creditCard = ContextManager.sharedInstance.contextCreditCard {
                self.navigationController?.pushViewController(CheckoutSelectBankViewController(selectedCreditCard: creditCard), animated: true)
            } else {
                let alert = UIAlertController(title: kPaymentSectionCardNotSelectedAlertTitle, message: kPaymentSectionCardNotSelectedAlertMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: kAlertButtonOK, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            })
        
        // Installment cell.
        var installmentsDisclosureText = kPaymentSectionSelectKey
        if let installmentsSelected = ContextManager.sharedInstance.contextInstallment {
            installmentsDisclosureText = installmentsSelected.paymentInstallmentMessage
        }
        
        let installmentsCellModel = PaymentCellModel(descriptionText: kPaymentSectionInstallmentsTitleKey, disclosureDescription: installmentsDisclosureText, creditCardImage: "", showDisclosure: true, action: { [unowned self] in
            if let bank = ContextManager.sharedInstance.contextBank {
                self.navigationController?.pushViewController(CheckoutSelectInstallmentsViewController(selectedCreditCard: ContextManager.sharedInstance.contextCreditCard, selectedBank: bank), animated: true)
            } else {
                let alert = UIAlertController(title: kPaymentSectionBankNotSelectedAlertTitle, message: kPaymentSectionBankNotSelectedAlertMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: kAlertButtonOK, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            })
        
        let paymentSectionList = [headerCellModel, creditCardCellModel, bankCellModel, installmentsCellModel]
        self.paymentSections.append(paymentSectionList)
    }
}
