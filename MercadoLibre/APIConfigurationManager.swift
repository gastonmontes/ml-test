//
//  APIConfigurationManager.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation

private let kAPIConfigurationFileName = "APIConfiguration"
private let kAPIConfigurationFileExtension = "plist"
private let kAPIBaseURLKey = "base_url"
private let kAPIPublicKeyKey = "public_key"

class APIConfigurationManager: NSObject {
    // MARK: - Singleton.
    static let sharedInstance = APIConfigurationManager()
    
    // MARK: - Configurations vars.
    private(set) var apiBaseURL: String!
    private(set) var apiPublicKey: String!
    
    // MARK: - Initialization.
    override init() {
        let confFilePath = Bundle.main.path(forResource: kAPIConfigurationFileName, ofType: kAPIConfigurationFileExtension)
        let confFileContent = NSDictionary(contentsOfFile: confFilePath!)!
        
        self.apiBaseURL = confFileContent.object(forKey: kAPIBaseURLKey) as? String
        self.apiPublicKey = confFileContent.object(forKey: kAPIPublicKeyKey) as? String
    }
}
