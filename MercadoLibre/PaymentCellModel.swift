//
//  PaymentCellModel.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

typealias ClosureAction = () -> (Void)

class PaymentCellModel {
    // MARK: - Vars.
    private(set) var paymentCellDescriptionText: String!
    private(set) var paymentCellImageName: String?
    private(set) var paymentCellDisclosureDescription: String?
    private(set) var paymentCellShowDisclosure: Bool!
    private(set) var paymentCellClousureAction: ClosureAction?
    
    // MARK: - Initialization functions.
    init(descriptionText: String) {
        self.paymentCellDescriptionText = descriptionText
        self.paymentCellShowDisclosure = false
    }
    
    init(descriptionText: String, disclosureDescription: String!, creditCardImage: String!, showDisclosure: Bool = false, action: @escaping ClosureAction) {
        self.paymentCellDescriptionText = descriptionText
        self.paymentCellImageName = creditCardImage
        self.paymentCellDisclosureDescription = disclosureDescription
        self.paymentCellShowDisclosure = showDisclosure
        self.paymentCellClousureAction = action
    }
}
