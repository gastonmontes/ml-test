//
//  CheckoutSelectCreditCardViewController.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

class CheckoutSelectCreditCardViewController: CheckoutBaseViewController {    
    // MARK: - Vars.
    private var creditCardsList = Array<PaymentMethod>()
    
    // MARK: - View lifecycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchCreditsCards()
    }
    
    // MARK: - Datasource.
    private func fetchCreditsCards() {
        self.view.loadingShow()
        DatasourceManager.datasourceGetCreditCardsForAmount(amount: ContextManager.sharedInstance.contextAmount, onComplete: { [unowned self] paymentMethodList, error in
            guard self.checkDatasource(datasourceList: paymentMethodList, error: error) else {
                self.view.loadingStop()
                return
            }
            
            self.creditCardsList = paymentMethodList!
            self.baseTableView!.reloadData()
            self.view.loadingStop()
            })
    }
    
    // MARK: - Table view reimplemented functions.
    override func cellModel(indexPath: NSIndexPath) -> PaymentCellModel {
        let creditCardModel = self.creditCardsList[indexPath.row]
        let cellModel = PaymentCellModel(descriptionText: creditCardModel.paymentMethodName,
                                         disclosureDescription: "",
                                         creditCardImage: creditCardModel.paymentMethodImageURL,
                                         showDisclosure: true,
                                         action: {})
        return cellModel
    }
    
    override func tableViewRowsNumber(section: Int) -> Int {
        return self.creditCardsList.count;
    }
    
    // MARK: - UITableViewDelegate implementation.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let creditCardSelected = self.creditCardsList[indexPath.row]
        
        let bankSelectionViewController = CheckoutSelectBankViewController(selectedCreditCard: creditCardSelected)
        self.navigationController?.pushViewController(bankSelectionViewController, animated: true)
    }
}
