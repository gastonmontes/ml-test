//
//  PaymentMethod.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import SwiftyJSON

enum PaymentMethodType: String {
    case CreditCard = "credit_card"
    case Ticket = "ticket"
    case Atm = "atm"
    case Unknown
}

private let kPaymentMethodIDKey = "id"
private let kPaymentMethodNameKey = "name"
private let kPaymentMethodImageURL = "secure_thumbnail"
private let kPaymentMethodTypeKey = "payment_type_id"
private let kPaymentMethodAmountMinKey = "min_allowed_amount"
private let kPaymentMethodAmountMaxKey = "max_allowed_amount"

class PaymentMethod {
    // MARK: - Vars.
    private(set) var paymentMethodID: String!
    private(set) var paymentMethodName: String!
    private(set) var paymentMethodImageURL: String!
    private(set) var paymentMethodAmountMin = Float.greatestFiniteMagnitude
    private(set) var paymentMethodAmountMax = Float.leastNormalMagnitude
    private(set) var paymentMethodType = PaymentMethodType.Unknown
    
    // MARK: - Initialization.
    init(json: JSON) {
        self.paymentMethodID = json[kPaymentMethodIDKey].stringValue
        self.paymentMethodName = json[kPaymentMethodNameKey].stringValue
        self.paymentMethodImageURL = json[kPaymentMethodImageURL].stringValue
        self.paymentMethodAmountMin = json[kPaymentMethodAmountMinKey].floatValue
        self.paymentMethodAmountMax = json[kPaymentMethodAmountMaxKey].floatValue
        
        if let paymentType = PaymentMethodType(rawValue: json[kPaymentMethodTypeKey].stringValue) {
            self.paymentMethodType = paymentType
        }
    }
}
