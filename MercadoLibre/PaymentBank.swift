//
//  PaymentBank.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import SwiftyJSON

private let kPaymentBankIDKey = "id"
private let kPaymentBankNameKey = "name"
private let kPaymentBankImageKey = "secure_thumbnail"

class PaymentBank {
    // MARK: - Vars.
    private(set) var paymentBankID: String!
    private(set) var paymentBankName: String!
    private(set) var paymentBankImage: String!
    
    // MARK: - Initialization.
    init(json: JSON) {
        self.paymentBankID = json[kPaymentBankIDKey].stringValue
        self.paymentBankName = json[kPaymentBankNameKey].stringValue
        self.paymentBankImage = json[kPaymentBankImageKey].stringValue
    }
}