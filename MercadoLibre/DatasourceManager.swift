//
//  DatasourceManager.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import SwiftyJSON

// TODO: Gastón - Hay que hacer un caché.

class DatasourceManager {
    /**
     * Function to get the maximum and minimum credits card limits.
     */
    class func datasourceGetAmountLimitsValues(onComplete: @escaping (_ minValue: Float?, _ maxValue: Float?, _ error: String?) -> Void) {
        self.datasourceGetAllCreditCards(onComplete: { paymentMethodList, error in
            guard error == nil else {
                onComplete(nil, nil, error!)
                return
            }
            
            var paymentMinValue = Float.greatestFiniteMagnitude
            var paymentMaxValue = Float.leastNormalMagnitude
            
            for paymentMethod in paymentMethodList! {
                if paymentMethod.paymentMethodAmountMin < paymentMinValue {
                    paymentMinValue = paymentMethod.paymentMethodAmountMin
                }
                
                if paymentMethod.paymentMethodAmountMax > paymentMaxValue {
                    paymentMaxValue = paymentMethod.paymentMethodAmountMax
                }
            }
            
            onComplete(paymentMinValue, paymentMaxValue, nil)
        })
    }
    
    class func getAllPaymentsMethods(onComplete: @escaping (_ paymentMethod: Array<PaymentMethod>?, _ error: String?) -> Void) {
        APIManager.apiHTTPGetRequest(relativePath: "", queryParameters: nil, onComplete: { jsonList, error in
            guard error == nil else {
                onComplete(nil, error)
                return
            }
            
            let jsonListJSONValue = JSON(jsonList!)
            
            var paymentMethodList = Array<PaymentMethod>()
            
            for paymentMethodJSON in jsonListJSONValue.arrayValue {
                let paymentMethod = PaymentMethod(json: paymentMethodJSON)
                paymentMethodList.append(paymentMethod)
            }
            
            onComplete(paymentMethodList, nil)
        })
    }
    
    /**
     * Function to get all the credit cards available to make a purchase.
     * Note: Only the credit cards, not ticket or atm.
     */
    class func datasourceGetAllCreditCards(onComplete: @escaping (_ paymentMethod: Array<PaymentMethod>?, _ error: String?) -> Void) {
        self.getAllPaymentsMethods(onComplete: { paymentList, error in
            guard error == nil else {
                onComplete(nil, error)
                return
            }
            
            var paymentMethodList = Array<PaymentMethod>()
            
            for paymentMethod in paymentList! {
                if paymentMethod.paymentMethodType == PaymentMethodType.CreditCard {
                    paymentMethodList.append(paymentMethod)
                }
            }
            
            onComplete(paymentMethodList, nil)
        })
    }
    
    /**
     * Function to get all the credit cards available to make a purchase with an especific amount.
     * Note: Only the credit cards which limit exceeds the amount passed as parameter, not ticket or atm.
     */
    class func datasourceGetCreditCardsForAmount(amount: Float!, onComplete: @escaping (_ paymentMethod: Array<PaymentMethod>?, _ error: String?) -> Void) {
        self.datasourceGetAllCreditCards(onComplete: { paymentMethodList, error in
            guard error == nil else {
                onComplete(nil, error)
                return
            }
            
            var creditCardsList = Array<PaymentMethod>()
            
            for paymentMethod in paymentMethodList! {
                if paymentMethod.paymentMethodAmountMin <= amount && paymentMethod.paymentMethodAmountMax >= amount {
                    creditCardsList.append(paymentMethod)
                }
            }
            
            onComplete(creditCardsList, nil)
        })
    }
    
    class func datasourceGetBanksForCreditCard(creditCardID: String!, onComplete: @escaping (_ paymentBank: Array<PaymentBank>?, _ error: String?) -> Void) {
        APIManager.apiHTTPGetRequest(relativePath: "card_issuers",
                                     queryParameters: ["payment_method_id": creditCardID as AnyObject],
                                     onComplete: { jsonList, error in
                                        guard error == nil else {
                                            onComplete(nil, error)
                                            return
                                        }
                                        
                                        let jsonListJSONValue = JSON(jsonList!)
                                        
                                        var banksList = Array<PaymentBank>()
                                        
                                        for bankJSON in jsonListJSONValue.arrayValue {
                                            let bank = PaymentBank(json: bankJSON)
                                            banksList.append(bank)
                                        }
                                        
                                        onComplete(banksList, nil)
        })
    }
    
    class func datasourceGetInstallments(amount: Float!, creditCardID: String!, issuerID: String!, onComplete: @escaping (_ paymentInstallment: Array<PaymentInstallment>?, _ error: String?) -> Void) {
        APIManager.apiHTTPGetRequest(relativePath: "installments",
                                     queryParameters: ["amount": amount as AnyObject, "payment_method_id": creditCardID as AnyObject, "issuer.id": issuerID as AnyObject],
                                     onComplete: { jsonList, error in
                                        guard error == nil else {
                                            onComplete(nil, error)
                                            return
                                        }
                                        
                                        let jsonListJSONValue = JSON(jsonList!)
                                        var installmentsList = Array<PaymentInstallment>()
                                        
                                        for informationDictionary in jsonListJSONValue.arrayValue {
                                            let paymentInstallments = informationDictionary["payer_costs"].arrayValue
                                            
                                            for installment in paymentInstallments {
                                                let installmentModel = PaymentInstallment(json: installment)
                                                installmentsList.append(installmentModel)
                                            }
                                        }
                                        
                                        onComplete(installmentsList, nil)
        })
    }
}
