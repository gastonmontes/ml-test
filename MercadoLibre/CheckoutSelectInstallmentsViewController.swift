//
//  CheckoutSelectInstallmentsViewController.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

class CheckoutSelectInstallmentsViewController: CheckoutBaseViewController {    
    // MARK: - Vars.
    private var selectedCreditCard: PaymentMethod!
    private var selectedBank: PaymentBank!
    private var installmentList = Array<PaymentInstallment>()
    
    // MARK: - Initialization.
    init(selectedCreditCard: PaymentMethod!, selectedBank: PaymentBank!) {
        super.init(nibName: nil, bundle: nil)
        
        self.selectedCreditCard = selectedCreditCard
        self.selectedBank = selectedBank
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View lifecycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchInstallments()
    }
    
    // MARK: - Data functions.
    private func fetchInstallments() {
        self.view.loadingShow()
        
        DatasourceManager.datasourceGetInstallments(amount: ContextManager.sharedInstance.contextAmount, creditCardID: self.selectedCreditCard.paymentMethodID, issuerID: self.selectedBank.paymentBankID, onComplete: { [unowned self] installmentList, error in
            guard self.checkDatasource(datasourceList: installmentList, error: error) else {
                self.view.loadingStop()
                return
            }
            
            self.installmentList = installmentList!
            self.view.loadingStop()
            self.baseTableView!.reloadData()
            })
    }
    
    // MARK: - UITableViewDelegate implementation.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let installmentModel = self.installmentList[indexPath.row]
        
        ContextManager.sharedInstance.contextManagerSetCreditCard(creditCard: self.selectedCreditCard)
        ContextManager.sharedInstance.contextManagerSetBank(bank: self.selectedBank)
        ContextManager.sharedInstance.contextManagerSetInstallments(installments: installmentModel)
        
        
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
    }
    
    // MARK: - Table view reimplemented functions.
    override func cellModel(indexPath: NSIndexPath) -> PaymentCellModel {
        let installmentModel = self.installmentList[indexPath.row]
        let cellModel = PaymentCellModel(descriptionText: installmentModel.paymentInstallmentMessage,
                                         disclosureDescription: "",
                                         creditCardImage: "",
                                         showDisclosure: true,
                                         action: {})
        return cellModel
    }
    
    override func tableViewRowsNumber(section: Int) -> Int {
        return self.installmentList.count;
    }
}
