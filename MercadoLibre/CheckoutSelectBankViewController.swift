//
//  CheckoutSelectBankViewController.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

class CheckoutSelectBankViewController: CheckoutBaseViewController {
    // MARK: - Vars.
    private var selectedCreditCard: PaymentMethod!
    private var paymentBanksList = Array<PaymentBank>()
    
    // MARK: - Initialization.
    init(selectedCreditCard: PaymentMethod!) {
        super.init(nibName: nil, bundle: nil)
        
        self.selectedCreditCard = selectedCreditCard
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View lifecycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchBanksData()
    }
    
    // MARK: - Data functions.
    private func fetchBanksData() {
        self.view.loadingShow()
        
        DatasourceManager.datasourceGetBanksForCreditCard(creditCardID: self.selectedCreditCard.paymentMethodID, onComplete: { [unowned self] bankList, error in
            guard self.checkDatasource(datasourceList: bankList, error: error) else {
                self.view.loadingStop()
                return
            }
            
            self.paymentBanksList = bankList!
            self.baseTableView!.reloadData()
            self.view.loadingStop()
            })
    }
    
    // MARK: - Table view reimplemented functions.
    override func cellModel(indexPath: NSIndexPath) -> PaymentCellModel {
        let bankModel = self.paymentBanksList[indexPath.row]
        let cellModel = PaymentCellModel(descriptionText: bankModel.paymentBankName,
                                         disclosureDescription: "",
                                         creditCardImage: bankModel.paymentBankImage,
                                         showDisclosure: true,
                                         action: {})
        return cellModel
    }
    
    override func tableViewRowsNumber(section: Int) -> Int {
        return self.paymentBanksList.count;
    }
    
    // MARK: - UITableViewDelegate implementation.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bankSelected = self.paymentBanksList[indexPath.row]
        
        let installmentsViewController = CheckoutSelectInstallmentsViewController(selectedCreditCard: self.selectedCreditCard, selectedBank: bankSelected)
        self.navigationController?.pushViewController(installmentsViewController, animated: true)
    }
}
