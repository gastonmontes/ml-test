//
//  APIManager.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import Alamofire

typealias APIParametersDictionaryType = Dictionary<String, AnyObject>
typealias APIResultType = Array<Dictionary<String, AnyObject>>
typealias APIServiceResponseType = (_ result: APIResultType?, _ error: String?) -> Void

private let kAPIPublicKeyKey = "public_key"

class APIManager {
    class func apiHTTPGetRequest(relativePath: String?, queryParameters: APIParametersDictionaryType?, onComplete: @escaping APIServiceResponseType) {
        var parameters = APIParametersDictionaryType()
        
        if queryParameters != nil && queryParameters!.count > 0 {
            parameters = queryParameters!
        }
        
        parameters.updateValue(APIConfigurationManager.sharedInstance.apiPublicKey as AnyObject, forKey: kAPIPublicKeyKey)
        
        var absolutURL = APIConfigurationManager.sharedInstance.apiBaseURL
        
        if relativePath != nil && relativePath!.count > 0 {
            absolutURL = absolutURL! + "/" + relativePath!
        }
        
        Alamofire.request(absolutURL!, method: .get, parameters: parameters)
            .validate()
            .responseJSON { (response) -> Void in
                // Check the response status.
                guard response.result.isSuccess else {
                    print("URL: " + response.request!.url!.absoluteString)
                    print(response.result.error!)
                    onComplete(nil, response.result.error!.localizedDescription)
                    return
                }
                
                // Check data type.
                guard let value = response.result.value as? APIResultType else {
                    print("URL: " + response.request!.url!.absoluteString)
                    print("Corrupted API data received")
                    print(response.result.value!)
                    onComplete(nil, "Corrupted API data received")
                    return
                }
                
                onComplete(value, nil)
        }
    }
}
