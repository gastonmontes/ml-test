//
//  InformationCell.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

private let kCellImageWidth = CGFloat(29)
private let kCellImageHeight = CGFloat(19)

class InformationCell: UITableViewCell {
    // MARK: - Constants.
    static private let kInformationCellHeight = CGFloat(24)
    static private let kSelectionCellHeight = CGFloat(40)
    
    // MARK: - Outlet vars.
    @IBOutlet private weak var separatorLine: UIView!
    @IBOutlet private weak var creditCardImage: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var disclosureImage: UIImageView!
    @IBOutlet private weak var disclosureDescription: UILabel!
    
    // MARK: - Constraint.
    @IBOutlet private weak var creditCardImageWidth: NSLayoutConstraint!
    @IBOutlet private weak var creditCardLeftSeparation: NSLayoutConstraint!
    @IBOutlet private weak var descriptionImageSeparation: NSLayoutConstraint!
    
    // MARK: - View life cycle.
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionLabel.textColor = UIColor.mlTextColor()
        self.disclosureDescription.textColor = UIColor.mlTextColor()
        self.separatorLine.backgroundColor = UIColor.mlGrayLightColor()
    }
    
    // MARK: - Class methods.
    class func cellHeight(cellModel: PaymentCellModel) -> CGFloat {
        if cellModel.paymentCellShowDisclosure == true {
            return kSelectionCellHeight
        }
        
        return kInformationCellHeight
    }
    
    // MARK: - Actions functions.
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Setters.
    private func setCreditCardImage(imageName: String?) {
        if imageName != nil && imageName!.count > 0 {
            UIImage.loadImageFromURL(imageURL: imageName, onComplete: { [weak self] cardImage, error in
                guard error == nil else {
                    self?.creditCardImageWidth.constant = 0
                    self?.descriptionImageSeparation.constant = 0
                    return
                }
                
                self?.creditCardImage.image = cardImage
                self?.creditCardImageWidth.constant = kCellImageWidth
                self?.descriptionImageSeparation.constant = 8
            })
        } else {
            self.creditCardImageWidth.constant = 0
            self.descriptionImageSeparation.constant = 0
        }
    }
    
    private func setDisclosure(cellModel: PaymentCellModel) {
        self.disclosureImage.isHidden = !cellModel.paymentCellShowDisclosure
        self.disclosureDescription.isHidden = !cellModel.paymentCellShowDisclosure
        self.isUserInteractionEnabled = cellModel.paymentCellShowDisclosure
        
        if cellModel.paymentCellShowDisclosure == true {
            self.disclosureDescription.text = cellModel.paymentCellDisclosureDescription
            self.descriptionLabel.font = UIFont.mlDetailFont()
        } else {
            self.descriptionLabel.font = UIFont.mlIniformationFont()
        }
    }
    
    // MARK: - Data functions.
    func informationCellSetData(cellModel: PaymentCellModel) {
        self.descriptionLabel.text = cellModel.paymentCellDescriptionText
        
        self.setDisclosure(cellModel: cellModel)
        self.setCreditCardImage(imageName: cellModel.paymentCellImageName)
    }
}
