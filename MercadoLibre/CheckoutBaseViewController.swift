//
//  CheckoutBaseViewController.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

private let kLocalizableTitleKey = ".title"

class CheckoutBaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: - IBOutlets vars.
    @IBOutlet weak var baseTableView: UITableView?
    
    // MARK: - Notifications handles.
    @objc func dataFetchError() {
        let alert = UIAlertController(title: kDataFetchErrorTitleKey, message: kDataFetchErrorMessageKey, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: kAlertButtonOK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dataFetchNoData() {
        let alert = UIAlertController(title: kDataFetchNoDataTitleKey, message: kDataFetchNoDataMessageKey, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: kAlertButtonOK, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataFetchError), name: NSNotification.Name(rawValue: kNotificationDataError), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataFetchNoData), name: NSNotification.Name(rawValue: kNotificationDataNoData), object: nil)
        
        self.setCustomNavigationBarStyle()
        self.setViewControllerProperties()
        self.setTableViewProperties()
    }
    
    // MARK: - UIViewController functions reimplementation.
    override var nibName: String? {
        get {
            return String(describing: type(of: self))
        }
    }
    
    override var nibBundle: Bundle? {
        get {
            return Bundle.main
        }
    }
    
    // MARK: - Styling functions.
    private func setCustomNavigationBarStyle() {
        self.navigationController?.navigationBar.barTintColor = UIColor.mlYellowColor()
        self.navigationController?.navigationBar.tintColor = UIColor.mlGrayDarkColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mlGrayDarkColor()]
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isOpaque = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    private func setTableViewProperties() {
        self.baseTableView?.backgroundColor = UIColor.mlGrayLightColor()
        
        self.baseTableView?.register(UINib(nibName: "InformationCell", bundle: Bundle.main), forCellReuseIdentifier: "InformationCell")
    }
    
    private func setViewControllerProperties() {
        self.view.backgroundColor = UIColor.mlGrayLightColor()
        self.title = String(String(describing: type(of: self)) + kLocalizableTitleKey).localize()
    }
    
    // MARK: - Table supporting methods.
    func cellModel(indexPath: NSIndexPath) -> PaymentCellModel {
        return PaymentCellModel(descriptionText: "")
    }
    
    func tableViewRowsNumber(section: Int) -> Int {
        return 0;
    }
    
    // MARK: - UITableViewDelegate implementation.
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kTableViewHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: kTableViewHeaderHeight))
        footerView.backgroundColor = UIColor.mlGrayLightColor()
        return footerView
    }
    
    // MARK: - UITableViewDataSource implementation.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InformationCell", for: indexPath) as! InformationCell
        
        let cellModel = self.cellModel(indexPath: indexPath as NSIndexPath)
        cell.informationCellSetData(cellModel: cellModel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewRowsNumber(section: section)
    }
    
    // MARK: Datasource functions
    func checkDatasource(datasourceList: Array<AnyObject>!, error: String!) -> Bool {
        guard error == nil else {
            self.navigationController?.popViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationDataError), object: nil)
            return false
        }
        
        guard datasourceList!.count > 0 else {
            self.navigationController?.popViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationDataNoData), object: nil)
            return false
        }
        
        return true
    }
}
