//
//  UIFont+ML.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func mlIniformationFont() -> UIFont {
        return UIFont.systemFont(ofSize: kFontSizeMedium)
    }
    
    class func mlDetailFont() -> UIFont {
        return UIFont.systemFont(ofSize: kFontSizeSmall)
    }
}
