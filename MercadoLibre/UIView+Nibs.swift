//
//  UIView+Nibs.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func loadViewFromNib(nibName: String!) -> UIView {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
