//
//  String+Localizable.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/27/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation

extension String {
    func localize() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
