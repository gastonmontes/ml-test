//
//  UIColor+ML.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/27/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func mlYellowColor() -> UIColor {
        return UIColor(red: 1.000, green: 0.902, blue: 0.000, alpha: 1.00)
    }
    
    class func mlGrayLightColor() -> UIColor {
        return UIColor(red:0.933, green:0.933, blue:0.933, alpha:1.00)
    }
    
    class func mlGrayMediumColor() -> UIColor {
        return UIColor(red:0.566, green:0.566, blue:0.566, alpha:1.00)
    }
    
    class func mlGrayDarkColor() -> UIColor {
        return UIColor(red:0.200, green:0.200, blue:0.200, alpha:1.00)
    }
    
    class func mlTextColor() -> UIColor {
        return UIColor(red:0.383, green:0.383, blue:0.383, alpha:1.00)
    }
    
    class func mlBlueColor() -> UIColor {
        return UIColor(red:0.204, green:0.514, blue:0.980, alpha: 1.00)
    }
}
