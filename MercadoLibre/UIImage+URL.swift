//
//  UIImage+URL.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

extension UIImage {
    class func loadImageFromURL(imageURL: String!, onComplete:@escaping (_ image: UIImage?, _ error: String?) -> Void) {
        Alamofire.request(imageURL, method: .get, parameters: nil)
            .validate()
            .responseData(completionHandler: { (response) -> Void in
                // Check the response status.
                guard response.result.isSuccess else {
                    print("URL: " + response.request!.url!.absoluteString)
                    print(response.result.error!)
                    onComplete(nil, response.result.error!.localizedDescription)
                    return
                }
                
                let image = UIImage(data: response.data!)
                onComplete(image, nil)
                })
    }
}
