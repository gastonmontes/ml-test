//
//  UIButton+Style.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/27/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func roundedCorner() -> Void {
        self.layer.cornerRadius = 4
    }
}