//
//  PaymentInstallment.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import SwiftyJSON

private let kPaymentInstallmentMinKey = "min_allowed_amount"
private let kPaymentInstallmentMaxKey = "max_allowed_amount"
private let kPaymentInstallmentRateKey = "installment_rate"
private let kPaymentInstallmentMessageKey = "recommended_message"
private let kPaymentInstallmentNumberKey = "installments"

class PaymentInstallment {
    // MARK: - Vars.
    private(set) var paymentInstallmentMinValue: Float!
    private(set) var paymentInstallmentMaxValue: Float!
    private(set) var paymentInstallmentRate: Float!
    private(set) var paymentInstallmentMessage: String!
    private(set) var paymentInstallmentNumber: Int!
    
    // MARK: - Initialization.
    init(json: JSON) {
        self.paymentInstallmentMinValue = json[kPaymentInstallmentMinKey].floatValue
        self.paymentInstallmentMaxValue = json[kPaymentInstallmentMaxKey].floatValue
        self.paymentInstallmentRate = json[kPaymentInstallmentRateKey].floatValue
        self.paymentInstallmentMessage = json[kPaymentInstallmentMessageKey].stringValue
        self.paymentInstallmentNumber = json[kPaymentInstallmentNumberKey].intValue
    }
}