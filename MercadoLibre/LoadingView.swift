//
//  LoadingView.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

private let kLoadingViewTag = 47365

class LoadingView: UIView {
    // MARK: - Creation functions.
    class func loadingView(frame: CGRect, backgrounColor: UIColor, alpha: CGFloat) -> LoadingView {
        let loadingView = UIView.loadViewFromNib(nibName: "LoadingView") as! LoadingView
        loadingView.frame = frame
        loadingView.backgroundColor = backgrounColor
        loadingView.alpha = alpha
        loadingView.tag = kLoadingViewTag
        
        loadingView.setEqualSizeAsSuperview()
        
        return loadingView
    }
    
    // MARK: - Getter functions.
    class func loadingViewTag() -> Int {
        return kLoadingViewTag
    }
    
    // MARK: - Constraint functions.
    private func setEqualSizeAsSuperview() {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalConstraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|",
                                                                                  options: [],
                                                                                  metrics: nil,
                                                                                  views: ["subview": self])
        superview.addConstraints(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|",
                                                                                options: [],
                                                                                metrics: nil,
                                                                                views: ["subview": self])
        superview.addConstraints(verticalConstraint)
    }
}
