//
//  UIView+Loading.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/28/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func loadingShow() {
        if self.loadingView() == nil {
            let loadingView = LoadingView.loadingView(frame: self.bounds, backgrounColor: UIColor.mlGrayLightColor(), alpha: 1)
            self.addSubview(loadingView)
        }
    }
    
    func loadingStop() {
        if let loadingView = self.loadingView() {
            loadingView.removeFromSuperview()
        }
    }
    
    private func loadingView() -> LoadingView? {
        let loadingView = self.viewWithTag(LoadingView.loadingViewTag()) as? LoadingView
        return loadingView
    }
}
