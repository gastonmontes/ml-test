//
//  Constants.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/3/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

let kDataFetchErrorTitleKey = "DataFetch.Error.title".localize()
let kDataFetchErrorMessageKey = "DataFetch.Error.message".localize()
let kDataFetchNoDataTitleKey = "DataFetch.NoData.title".localize()
let kDataFetchNoDataMessageKey = "DataFetch.NoData.message".localize()
let kAlertButtonOK = "OK"

let kNotificationDataError = "Notification.Data.Error"
let kNotificationDataNoData = "Notification.Data.NoData"
let kNotificationPaymentSelected = "Notification.Payment.Selected"

let kFontSizeMedium = CGFloat(17)
let kFontSizeSmall = CGFloat(14)

let kButtonHeight = CGFloat(40)
let kTableViewFooterHeight = CGFloat(64)
let kTableViewHeaderHeight = CGFloat(16)
let kMargin = CGFloat(8)
