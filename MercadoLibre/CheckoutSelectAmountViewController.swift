//
//  CheckoutSelectAmountViewController.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 5/26/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import UIKit

private let kViewControllerTitleKey = "CheckoutSelectAmountViewController.title".localize()
private let kBuyButtonTitleKey = "CheckoutSelectAmountViewController.BuyButton.title".localize()
private let kBuyButtonChangeTitleKey = "CheckoutSelectAmountViewController.BuyButton.Change.title".localize()
private let kSliderAlertTitleKey = "CheckoutSelectAmountViewController.SliderAmount.Alert.title".localize()
private let kSliderAlertMessageKey = "CheckoutSelectAmountViewController.SliderAmount.Alert.message".localize()
private let kOperationConfirmedTitle = "CheckoutSelectAmountViewController.OperationFinished.Alert.title".localize()
private let kOperationConfirmedAmountMessage = "CheckoutSelectAmountViewController.OperationFinished.Alert.Amount.message".localize()
private let kOperationConfirmedCreditCardMessage = "CheckoutSelectAmountViewController.OperationFinished.Alert.CreditCard.message".localize()
private let kOperationConfirmedBankMessage = "CheckoutSelectAmountViewController.OperationFinished.Alert.Bank.message".localize()
private let kOperationConfirmedInstallmentMessage = "CheckoutSelectAmountViewController.OperationFinished.Alert.Installment.message".localize()

class CheckoutSelectAmountViewController: CheckoutBaseViewController {
    // MARK: - Outlets components.
    @IBOutlet private weak var buyButton: UIButton!
    @IBOutlet private weak var amountSlider: UISlider!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet private weak var amountMaxLabel: UILabel!
    @IBOutlet private weak var amountMinLabel: UILabel!
    
    // MARK: - Vars.
    private var backAction: (() -> (Void))?
    
    // MARK: - Initialization.
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    init(backAction: @escaping () -> (Void)) {
        super.init(nibName: nil, bundle: nil)
        
        self.backAction = backAction
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(confirmPayment), name: NSNotification.Name(rawValue: kNotificationPaymentSelected), object: nil)
        
        self.setBuyButtonProperties()
        self.setAmountLabelsProperties()
        self.setAmountSliderProperties()
        
        self.getMaximumAndMinimumValues()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.amountSlider.maximumValue > 0 {
            self.setSliderMaximumAndMinimumValues(minimum: self.amountSlider.minimumValue, maximum: self.amountSlider.maximumValue)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Styling functions.
    private func setBuyButtonProperties() {
        self.buyButton.roundedCorner()
        self.buyButton.backgroundColor = UIColor.mlBlueColor()
        
        if self.backAction != nil {
            self.buyButton.setTitle(kBuyButtonChangeTitleKey, for: UIControl.State.normal)
        } else {
            self.buyButton.setTitle(kBuyButtonTitleKey, for: UIControl.State.normal)
        }
    }
    
    private func setAmountSliderProperties() {
        self.amountSlider.minimumTrackTintColor = UIColor.mlGrayMediumColor()
        self.amountSlider.maximumTrackTintColor = UIColor.mlGrayMediumColor()
        self.amountSlider.thumbTintColor = UIColor.mlGrayMediumColor()
    }
    
    private func setAmountLabelsProperties() {
        self.amountLabel.textColor = UIColor.mlGrayMediumColor()
        self.amountMaxLabel.textColor = UIColor.mlGrayMediumColor()
        self.amountMinLabel.textColor = UIColor.mlGrayMediumColor()
    }
    
    private func setSliderMaximumAndMinimumValues(minimum: Float, maximum: Float) {
        self.amountSlider.maximumValue = maximum
        self.amountSlider.minimumValue = minimum
        self.amountSlider.value = (maximum + minimum) / 2
        
        if ContextManager.sharedInstance.contextAmount != nil && ContextManager.sharedInstance.contextAmount! > 0 {
            self.amountSlider.value = ContextManager.sharedInstance.contextAmount!
        }
        
        self.amountLabel.text = "$" + String(Int(self.amountSlider.value))
        self.amountMaxLabel.text = "$\(Int(self.amountSlider.maximumValue))"
        self.amountMinLabel.text = "$\(Int(self.amountSlider.minimumValue))"
    }
    
    // MARK: - Actions funtions.
    @objc func confirmPayment() {
        let amountMessage = kOperationConfirmedAmountMessage + "$" + "\(Int(ContextManager.sharedInstance.contextAmount!))" + ".\n"
        let creditCardMessage = kOperationConfirmedCreditCardMessage + ContextManager.sharedInstance.contextCreditCard!.paymentMethodName + ".\n"
        let bankMessage = kOperationConfirmedBankMessage + ContextManager.sharedInstance.contextBank!.paymentBankName + ".\n"
        let installmentMesasge = kOperationConfirmedInstallmentMessage + ContextManager.sharedInstance.contextInstallment!.paymentInstallmentMessage + ".\n"
        let finalMessage = amountMessage + creditCardMessage + bankMessage + installmentMesasge
        
        let alert = UIAlertController(title: kOperationConfirmedTitle, message: finalMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: kAlertButtonOK, style: UIAlertAction.Style.default, handler: { [unowned self] action in
            ContextManager.sharedInstance.contextManagerReset()
            self.getMaximumAndMinimumValues()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func buyButtonAction(sender: UIButton) {
        if self.amountSlider.value <= 0 {
            let alert = UIAlertController(title: kSliderAlertTitleKey, message: kSliderAlertMessageKey, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: kAlertButtonOK, style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let sliderAmount = self.amountSlider.value
        ContextManager.sharedInstance.contextManagerSetAmount(amount: sliderAmount)
        
        if self.backAction != nil {
            self.resetContextIfNeeded(newValue: sliderAmount, onComplete: { [unowned self] in
                self.backAction!()
                })
        } else {
            self.navigationController?.pushViewController(CheckoutPaymentViewController(), animated: true)
        }
    }
    
    @IBAction private func amountSliderValueChanged(sender: UISlider) {
        self.amountLabel.text = "$" + String(Int(self.amountSlider.value))
    }
    
    // MARK: - Context funtions.
    private func resetContextIfNeeded(newValue: Float, onComplete: @escaping () -> (Void)) {
        if let paymentMethod = ContextManager.sharedInstance.contextCreditCard, let installment = ContextManager.sharedInstance.contextInstallment {
            if newValue > paymentMethod.paymentMethodAmountMax ||
                newValue < paymentMethod.paymentMethodAmountMin ||
                newValue > installment.paymentInstallmentMaxValue ||
                newValue < installment.paymentInstallmentMinValue {
                // Reset the context because is not longer valid.
                ContextManager.sharedInstance.contextManagerResetPayment()
                onComplete()
            } else {
                // The context is valid but must reselect the installments again.
                self.fetchNewPaymentInstallments(onComplete: onComplete)
            }
        } else {
            onComplete()
        }
    }
    
    private func fetchNewPaymentInstallments(onComplete: @escaping () -> (Void)) {
        self.view.loadingShow()
        
        let paymentAmount = ContextManager.sharedInstance.contextAmount!
        let paymentMethod = ContextManager.sharedInstance.contextCreditCard!
        let paymentBank = ContextManager.sharedInstance.contextBank!
        let paymentInstallment = ContextManager.sharedInstance.contextInstallment!
        
        DatasourceManager.datasourceGetInstallments(amount: paymentAmount, creditCardID: paymentMethod.paymentMethodID, issuerID: paymentBank.paymentBankID, onComplete: { [unowned self] installmentList, error in
            guard self.checkDatasource(datasourceList: installmentList, error: error) else {
                self.view.loadingStop()
                return
            }
            
            ContextManager.sharedInstance.contextManagerResetInstallment()
            
            for installment in installmentList! {
                if paymentInstallment.paymentInstallmentNumber == installment.paymentInstallmentNumber ||
                installmentList!.count == 1 {
                    ContextManager.sharedInstance.contextManagerSetInstallments(installments: installment)
                }
            }
            
            self.view.loadingStop()
            onComplete()
            })
    }
    
    // MARK: - Datasource funtions.
    private func getMaximumAndMinimumValues() {
        self.view.loadingShow()
        DatasourceManager.datasourceGetAmountLimitsValues(onComplete: {[unowned self] minValue, maxValue, error in
            guard error == nil else {
                self.view.loadingStop()
                let alert = UIAlertController(title: kDataFetchErrorTitleKey, message: kDataFetchErrorMessageKey, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: kAlertButtonOK, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            guard minValue != nil && maxValue != nil else {
                self.view.loadingStop()
                let alert = UIAlertController(title: kDataFetchNoDataTitleKey, message: kDataFetchNoDataMessageKey, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: kAlertButtonOK, style: .default, handler:
                    { [unowned self] action in
                        self.navigationController?.popViewController(animated: true)
                    }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.setSliderMaximumAndMinimumValues(minimum: minValue!, maximum: maxValue!)
            self.view.loadingStop()
            })
    }
}
