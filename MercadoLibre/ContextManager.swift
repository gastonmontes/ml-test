//
//  ContextManager.swift
//  MercadoLibre
//
//  Created by Gaston Montes on 6/2/16.
//  Copyright © 2016 Gaston Montes. All rights reserved.
//

import Foundation

class ContextManager {
    // MARK: - Singleton.
    static let sharedInstance = ContextManager()
    
    // MARK: - Vars.
    private(set) var contextAmount: Float?
    private(set) var contextCreditCard: PaymentMethod?
    private(set) var contextBank: PaymentBank?
    private(set) var contextInstallment: PaymentInstallment?
    
    // MARK: - Setters.
    func contextManagerSetAmount(amount: Float) {
        self.contextAmount = amount
    }
    
    func contextManagerSetCreditCard(creditCard: PaymentMethod) {
        self.contextCreditCard = creditCard
    }
    
    func contextManagerSetBank(bank: PaymentBank) {
        self.contextBank = bank
    }
    
    func contextManagerSetInstallments(installments: PaymentInstallment) {
        self.contextInstallment = installments
    }
    
    // MARK: - Data functions.
    func contextManagerReset() {
        self.contextAmount = 0
        self.contextCreditCard = nil
        self.contextBank = nil
        self.contextInstallment = nil
    }
    
    func contextManagerResetPayment() {
        self.contextCreditCard = nil
        self.contextBank = nil
        self.contextInstallment = nil
    }
    
    func contextManagerResetInstallment() {
        self.contextInstallment = nil
    }
}